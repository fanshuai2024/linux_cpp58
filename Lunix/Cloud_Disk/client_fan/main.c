#include "client.h"
#define MAX_CONNEXT 1024

int main()
{
    int clientfd = socket(AF_INET, SOCK_STREAM, 0);
    ERROR_CHECK(clientfd, -1, "socket");

    struct sockaddr_in serveraddr;
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(8080);
    inet_pton(AF_INET, "192.168.45.129", &serveraddr.sin_addr);

    int ret = connect(clientfd, (const struct sockaddr *)&serveraddr, sizeof(serveraddr));
    ERROR_CHECK(ret, -1, "connect");

    int epfd = epoll_create1(0);
    ERROR_CHECK(epfd, -1, "epoll_create1");

    addEpollReadfd(epfd, clientfd);
    addEpollReadfd(epfd, STDIN_FILENO);

    struct epoll_event *pEventAddr = calloc(MAX_CONNEXT, sizeof(struct epoll_event));
    char buff[1000] = {0};
    while (1)
    {
        int nreadys = epoll_wait(epfd, pEventAddr, MAX_CONNEXT, -1);
        switch (nreadys)
        {
        case -1:
            ERROR_CHECK(nreadys, -1, "epoll_wait");
            break;
        default:
            for (size_t i = 0; i < nreadys; i++)
            {
                int fd = pEventAddr->data.fd;
                if (fd == STDIN_FILENO)
                {
                    memset(buff, 0, sizeof(buff));
                    int ret = read(fd, buff, sizeof(buff));
                    ERROR_CHECK(ret, -1, "read");
                    if (ret)
                    {
                        return;
                    }
                    // 解析命令。
                    train_t t = pars_command(buff);

                    // 发送命令
                    // 先发送长度
                    sendn(clientfd, &t.len, 4);
                    // 在发送内容
                    sendn(clientfd, &t, 4 + t.len);
                }
                if (/* condition */)
                {
                    /* code */
                }
                
            }
        }
    }
    close(clientfd);

    return 0;
}
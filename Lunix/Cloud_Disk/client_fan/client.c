#include "client.h"

int addEpollReadfd(int epfd, int fd)
{
    struct epoll_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.events = EPOLLIN;
    ev.data.fd = fd;
    int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev);
    ERROR_CHECK(ret, -1, "epoll_ctl");
    return 0;
}

// 其作用：确定发送len字节的数据
int sendn(int sockfd, const void *buff, int len)
{
    int left = len;
    const char *pbuf = buff;
    int ret = -1;
    while (left > 0)
    {
        ret = send(sockfd, pbuf, left, 0);
        if (ret < 0)
        {
            perror("send");
            return -1;
        }

        left -= ret;
        pbuf += ret;
    }
    return len - left;
}

// 其作用：确定接收len字节的数据
int recvn(int sockfd, void *buff, int len)
{
    int left = len; // 还剩下多少个字节需要接收
    char *pbuf = buff;
    int ret = -1;
    while (left > 0)
    {
        ret = recv(sockfd, pbuf, left, 0);
        if (ret == 0)
        {
            break;
        }
        else if (ret < 0)
        {
            perror("recv");
            return -1;
        }

        left -= ret;
        pbuf += ret;
    }
    // 当退出while循环时，left的值等于0
    return len - left;
}

// 假设max_tokens是数组tokens的最大大小
// 在使用后，记得要释放空间
void splitString(const char *pstrs, char *tokens[], int max_tokens, int *pcount)
{
    int token_count = 0;
    char *token = strtok((char *)pstrs, " "); // 使用空格作为分隔符

    while (token != NULL && token_count < max_tokens - 1)
    { // 保留一个位置给NULL终止符
        // strlen 函数用于计算字符串的长度，但不包括字符串末尾的 null 终止符 '\0'。
        // 因此，如果您想要为字符串分配足够的内存以存储所有的字符以及末尾的 null 终止符，
        // 您需要将 strlen 函数返回的长度加 1。
        char *pstr = (char *)calloc(1, strlen(token) + 1);
        strcpy(pstr, token);
        tokens[token_count] = pstr; // 保存申请的堆空间首地址
        token_count++;
        token = strtok(NULL, " "); // 继续获取下一个token
    }
    // 添加NULL终止符
    tokens[token_count] = NULL;
    *pcount = token_count;
}

void freeStrs(char *pstrs[], int count)
{
    for (int i = 0; i < count; ++i)
    {
        free(pstrs[i]);
    }
}

train_t pars_command(char *buff)
{
    // 分割字符串
    char *strs[100] = {0};
    int cnt = 0;
    splitString(buff, strs, 100, &cnt);
    train_t t;
    memset(&t, 0, sizeof(t));
    // 根据命令选择cmdType
    char *cmd = strs[0];
    if (strcmp(cmd, "pwd") == 0)
    {
        t.type = CMD_TYPE_PWD;
    }
    else if (strcmp(cmd, "ls") == 0)
    {
        t.type = CMD_TYPE_LS;
    }
    else if (strcmp(cmd, "cd") == 0)
    {
        t.type = CMD_TYPE_CD;
    }
    else if (strcmp(cmd, "mkdir") == 0)
    {
        t.type = CMD_TYPE_MKDIR;
    }
    else if (strcmp(cmd, "rmdir") == 0)
    {
        t.type = CMD_TYPE_RMDIR;
    }
    else if (strcmp(cmd, "puts") == 0)
    {
        t.type = CMD_TYPE_PUTS;
    }
    else if (strcmp(cmd, "gets") == 0)
    {
        t.type = CMD_TYPE_GETS;
    }
    else
    {
        t.type = CMD_TYPE_NOTCMD;
        freeStrs(strs, cnt);
        printf("错误命令\n");
        exit(1);
    }

    // 填充命令后的的长度，内容 eror 重写
    for (size_t i = 1; i < cnt; i++)
    {
        t.len = strlen(strs[i]) + 4;
        strcpy(t.buff, strs[i]);
    }

    freeStrs(strs, cnt);
    return t;
}

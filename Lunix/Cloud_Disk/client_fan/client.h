#pragma once

#include <func.h>

typedef enum {
    CMD_TYPE_PWD=1,
    CMD_TYPE_LS,
    CMD_TYPE_CD,
    CMD_TYPE_MKDIR,
    CMD_TYPE_RMDIR,
    CMD_TYPE_PUTS,
    CMD_TYPE_GETS,
    CMD_TYPE_NOTCMD  //不是命令
}CmdType;

typedef struct
{
    int len;
    CmdType type;
    char buff[1000];
}train_t;

int addEpollReadfd(int epfd, int fd);
int sendn(int sockfd, const void * buff, int len);
int recvn(int sockfd, void * buff, int len);
train_t pars_command(char *buff);
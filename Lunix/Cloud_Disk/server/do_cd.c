#include "thread_pool.h"

void cdCommand(task_t *task)
{
    const char *clientDir = task->data;

    // 确保服务器有权限访问该目录
    if (access(clientDir, F_OK) != 0)
    {
        printf("Error: No such directory or insufficient permissions\n");
        return;
    }

    int ret = chdir(clientDir);
    if (ret == -1)
    {
        error(1, errno, "chdir");
    }

    char *pwd = getcwd(NULL, 0);
    if (pwd == NULL)
    {
        free(pwd);
        error(1, errno, "getcwd");
    }

    sendn(task->peerfd, pwd, strlen(pwd) + 1);

    // 释放 getcwd 分配的内存
    free(pwd);

    printf("execute cd command.\n");
}

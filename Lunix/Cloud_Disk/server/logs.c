#include "thread_pool.h"

#define MAX_LOG_SIZE 2048 // 假设日志文件的最大大小为 2048 字节

int logs_client_requst(const struct sockaddr_in *client_addr, task_t *ptask)
{
    // 创建日志文件
    int log_file_fd = open("/home/fan/GP_NetDisk/Cloud_Disk/server/log.txt", O_CREAT | O_RDWR | O_APPEND, 0666);
    ERROR_CHECK(log_file_fd, -1, "open");

    char buff[MAX_LOG_SIZE] = {0};
    if (client_addr != NULL)
    {
        char client_ip[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &client_addr->sin_addr, client_ip, sizeof(client_ip));      
        int client_port = ntohs(client_addr->sin_port);

        // 获取当前时间
        time_t now = time(NULL);
        // 讲time_t转化为tm
        struct tm *local = localtime(&now);
        // 获得时间
        int year = local->tm_year + 1900;
        int month = local->tm_mon + 1;
        int day = local->tm_mday;
        int hour = local->tm_hour;
        int min = local->tm_min;
        int second = local->tm_sec;

        sprintf(buff, "%d %d %d %d:%d:%d form %s %d\n", year, month, day, hour, min, second, client_ip, client_port);

        write(log_file_fd, buff, strlen(buff));
    }

    if (ptask != NULL)
    {
        //printf("logs opera!\n");
        // 获取当前时间
        time_t now = time(NULL);
        // 讲time_t转化为tm
        struct tm *local = localtime(&now);
        // 获得时间
        int year = local->tm_year + 1900;
        int month = local->tm_mon + 1;
        int day = local->tm_mday;
        int hour = local->tm_hour;
        int min = local->tm_min;
        int second = local->tm_sec;
        //printf("logs time!\n");

        char opra[10] = {0};
        switch (ptask->type)
        {
        case CMD_TYPE_PWD:
            strcpy(opra, "pwd");
            break;
        case CMD_TYPE_CD:
            strcpy(opra, "cd");
            break;
        case CMD_TYPE_LS:
            strcpy(opra, "ls");
            break;
        case CMD_TYPE_MKDIR:
            strcpy(opra, "mkdir");
            break;
        case CMD_TYPE_RMDIR:
            strcpy(opra, "rmdir");
            break;
        case CMD_TYPE_NOTCMD:
            strcpy(opra, "notcmd");
            break;
        case CMD_TYPE_PUTS:
            strcpy(opra, "puts");
            // 结束后要重新添加到epfd中
            break;
        case CMD_TYPE_GETS:
            strcpy(opra, "gets");
            break;
        }

        sprintf(buff, "%d %d %d %d:%d:%d form %s %s\n", year, month, day, hour, min, second, opra, ptask->data);
        //printf("%s\n", buff);
        int ret = write(log_file_fd, buff, strlen(buff));
        //printf("%d\n",ret);
        ERROR_CHECK(ret, -1, "write opera");
    }

    close(log_file_fd);
    //printf("....\n");

    return 0;
}

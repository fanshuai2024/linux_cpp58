#include "thread_pool.h"

void putsCommand(task_t *task)
{
    // 先接受文件名
    char *filename = task->data;

    // 创建文件
    int fd = open(filename, O_RDWR | O_CREAT, 0664);
    if (fd == -1)
    {
        error(-1, errno, "open\n");
    }

    char buff[1000] = {0};

    // 接受文件内容
    while (1)
    {
        memset(buff, 0, sizeof(buff));

        // 先接文件长度
        int len;
        recvn(task->peerfd, &len, 4);

        // 发送的小火车中有一个CmdType，要先接下来
        CmdType type;
        recvn(task->peerfd, &type, 4);

        // 再接文件内容
        int ret = recvn(task->peerfd, buff, len);

        // 把文件内容写到文件里
        write(fd, buff, ret);
    }

    close(fd);

    printf("execute puts command.\n");
}
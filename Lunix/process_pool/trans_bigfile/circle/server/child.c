#include "process_pool.h"

int makeChild(process_data_t *p, int num)
{

    for (size_t i = 0; i < num; i++)
    {
        int pipefds[2];
        socketpair(AF_LOCAL, SOCK_STREAM, 0, pipefds);

        pid_t pid = fork();
        if (pid == 0)
        {
            close(pipefds[1]);
            handleTask(pipefds[0]);
            //子进程要退出要不然会变成孤儿进程
            exit(0);
        }

        close(pipefds[0]);
        printf("child %d \n", pid);

        p->pid = pid;
        p->pipefd = pipefds[1];
        p->status = FREE;
    }

    return 0;
}

int handleTask(int pipefd)
{
    printf("child handle task.\n");

    while (1)
    {
        int peerfd;
        recvFd(pipefd, &peerfd);

        transferFile(peerfd);

        close(peerfd);

        int ONE = 1;

        write(pipefd, &ONE, sizeof(ONE));
    }

    return 0;
}
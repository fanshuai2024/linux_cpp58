#include "process_pool.h"

#define FILENAME "bigfile.avi"

int sendn(int sockfd, const void * buff, int len)
{
    int left = len;
    const char * pbuf = buff;
    int ret = 0;
    while(left > 0) {
        ret = send(sockfd, pbuf, left, 0);
        if(ret == -1) {
            perror("send");
            return -1;
        }
        pbuf += ret;
        left -= ret;
    }
    return len - left;
}

int transferFile(int peerfd)
{
    //读取本地文件
    int fd = open(FILENAME, O_RDWR);
    ERROR_CHECK(fd, -1, "open");

    train_t t;
    memset(&t, 0, sizeof(t));
    //先发送文件名
    t.len = strlen(FILENAME);
    strcpy(t.buff, FILENAME);
    send(peerfd, &t, 4 + t.len, 0);

    //其次发送文件长度，可以不用小火车，因为长度是确定的off_t大小。
    struct stat st;
    memset(&st, 0, sizeof(st));
    fstat(fd, &st);
    printf("filelength: %ld\n", st.st_size);//off_t
    printf("sizeof(st.st_size): %ld\n", sizeof(st.st_size));
    send(peerfd, &st.st_size, sizeof(st.st_size), 0);
    
    //最后发送文件内容
    while(1) {
        
        memset(&t, 0, sizeof(t));
        int ret = read(fd, t.buff, sizeof(t.buff));
        if(ret != 1000) {
            printf("read ret: %d\n", ret);
        }
        //如果不写文件读完之后退出，会导致一直循环发送0；
        if(ret == 0) {
            //文件已经读取完毕
            break;
        }
        t.len = ret;
        ret = sendn(peerfd, &t, 4 + t.len);
        //一定要检查ret，因为客户端在还没接受完文件就突然关闭了之后，
       // sendn发生错误，返回-1，如果不处理就会陷入while（1）的死循环
        if(ret == -1) {
            break;
        }
        if(ret != 1004) {
            printf("send ret: %d\n", ret);
        }
    }
    printf("send file over.\n");

    close(fd);//关闭文件
    return 0;
}


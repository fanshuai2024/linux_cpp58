#include <func.h>

int main()
{
    int clientfd = socket(AF_INET, SOCK_STREAM, 0);
    ERROR_CHECK(clientfd, -1, "socket");

    struct sockaddr_in serveraddr;
    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(8080);
    inet_pton(AF_INET, "192.168.45.129", &serveraddr.sin_addr);

    int ret = connect(clientfd, (const struct sockaddr *)&serveraddr, sizeof(serveraddr));
    ERROR_CHECK(ret, -1, "connect");

    int len = 0;
    recv(clientfd, &len, sizeof(len), 0);

    char fileName[100];
    recv(clientfd, fileName, len, 0);
    printf("filename:%s\n", fileName);

    int fd = open(fileName, O_RDWR | O_CREAT, 0666);
    ERROR_CHECK(fd,-1,"open");



    char buff[1000];
    recv(clientfd, &len, sizeof(len), 0);
    ret = recv(clientfd, buff, len, 0);
    printf("%s\n", buff);

    write(fd, buff, ret);

    close(fd);

    return 0;
}
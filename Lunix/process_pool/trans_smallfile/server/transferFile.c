#include "process_pool.h"

#define FILENAME "small_file.txt"

int transferFile(int peerfd)
{

    int fd = open(FILENAME, O_RDWR);
    ERROR_CHECK(fd, -1, "open");

    train_t t;
    bzero(&t, sizeof(t));

    // 先发送文件名
    t.len = strlen(FILENAME);
    strcpy(t.buff, FILENAME);
    send(peerfd, &t, 4 + t.len, 0);

    // 在发送文件内容
    bzero(&t, sizeof(t));
    char buf[1000] = {0};
    int ret = read(fd, buf, sizeof(buf));
    t.len = ret;
    strncpy(t.buff, buf, ret);
    send(peerfd, &t, 4 + t.len, 0);

    close(fd);

    return 0;
}
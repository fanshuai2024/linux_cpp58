#include "process_pool.h"

int main(int argc, char *argv[])
{

    //./server ip port processNum
    ARGS_CHECK(argc, 4);

    int processNum = atoi(argv[3]);
    process_data_t *pProcess = calloc(processNum, sizeof(process_data_t));

    makeChild(pProcess, processNum);

    int listenfd = tcpInit(argv[1], argv[2]);

    int epfd = epoll_create1(0);
    ERROR_CHECK(epfd, -1, "epoll_create1");

    epollAddFd(epfd, listenfd);

    for (size_t i = 0; i < processNum; i++)
    {
        epollAddFd(epfd, pProcess[i].pipefd);
    }

    struct epoll_event *pEventArr = (struct epoll_event *)calloc(EVENT_ARR_SIZE, sizeof(struct epoll_event));
    while (1)
    {
        int nready = epoll_wait(epfd, pEventArr, EVENT_ARR_SIZE, 5000);

        for (size_t i = 0; i < nready; i++)
        {
            int fd = pEventArr[i].data.fd;
            if (fd == listenfd)
            {
                int peerfd = accept(listenfd, NULL, NULL);

                for (size_t j = 0; j < processNum; j++)
                {
                    if (pProcess[j].status == FREE)
                    {
                        sendFd(pProcess[j].pipefd, peerfd);
                        pProcess[j].status = BUSY;
                        break;
                    }
                }

                // 一定要子进程和父进程都关闭peerfd。才能真正的关闭
                close(peerfd); // 一定要加不然会进入close_wait状态。
            }
            else
            {
                int howmany = 0;
                read(fd, &howmany, sizeof(howmany));
                for (size_t j = 0; j < processNum; j++)
                {
                    if (pProcess[j].pipefd == fd)
                    {
                        pProcess[j].status = FREE;
                        break;
                    }
                }
            }
        }
    }

    return 0;
}
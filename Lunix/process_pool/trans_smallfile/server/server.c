#include "process_pool.h"

int tcpInit(const char *ip, const char *port)
{

    int listenfd = socket(AF_INET, SOCK_STREAM, 0);
    ERROR_CHECK(listenfd, -1, "socket");

    struct sockaddr_in serveraddr;
    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(port));
    inet_pton(AF_INET, ip, &serveraddr.sin_addr);

    int on = 1;
    int ret = setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
    ERROR_CHECK(ret, -1, "setsockopt");

    ret = bind(listenfd, (const struct sockaddr *)&serveraddr, sizeof(serveraddr));
    ERROR_CHECK(ret, -1, "bind");

    ret = listen(listenfd, 10);
    ERROR_CHECK(ret, -1, "listen");
    return listenfd;
}

int epollAddFd(int epfd, int fd)
{
    struct epoll_event ev;
    bzero(&ev, sizeof(ev));
    ev.events = EPOLLIN;
    ev.data.fd = fd;

    int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev);
    ERROR_CHECK(ret, -1, "epoll_ctl");

    return 0;
}

int epollDelFd(int epfd, int fd)
{
    struct epoll_event ev;
    bzero(&ev, sizeof(ev));
    ev.events = EPOLLIN;
    ev.data.fd = fd;

    int ret = epoll_ctl(epfd, EPOLL_CTL_DEL, fd, &ev);
    ERROR_CHECK(ret, -1, "epoll_ctl");

    return 0;
}